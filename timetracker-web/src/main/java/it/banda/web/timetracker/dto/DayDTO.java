package it.banda.web.timetracker.dto;

import java.io.Serializable;
import java.sql.Date;

public class DayDTO implements Serializable {
	private static final long serialVersionUID = 79014459887442519L;
	
	private Integer hoursOfWork;
	private Date date;
	private String note;
	
	public DayDTO() {
	}

	public DayDTO(Integer hoursOfWork, Date date, String note) {
		this.hoursOfWork = hoursOfWork;
		this.date = date;
		this.note = note;
	}

	public Integer getHoursOfWork() {
		return hoursOfWork;
	}

	public void setHoursOfWork(Integer hoursOfWork) {
		this.hoursOfWork = hoursOfWork;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}
