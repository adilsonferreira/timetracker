package it.banda.web.timetracker.dto;

import java.io.Serializable;
import java.util.List;

public class ActivityDTO implements Serializable {
	private static final long serialVersionUID = 2964272707328261046L;
	
	private Integer id;
	private String description;
	private List<DayDTO> dayList;
	
	public ActivityDTO() {
	}

	public ActivityDTO(Integer id, String description, List<DayDTO> dayList) {
		this.id = id;
		this.description = description;
		this.dayList = dayList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DayDTO> getDayList() {
		return dayList;
	}

	public void setDayList(List<DayDTO> dayList) {
		this.dayList = dayList;
	}
	
}
