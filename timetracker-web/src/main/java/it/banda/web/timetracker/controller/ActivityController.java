package it.banda.web.timetracker.controller;

import it.banda.web.timetracker.dto.ActivityListDTO;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Activity;
import it.banda.web.timetracker.model.Allocation;
import it.banda.web.timetracker.model.Role;
import it.banda.web.timetracker.model.User;
import it.banda.web.timetracker.service.ActivityService;
import it.banda.web.timetracker.service.AllocationService;
import it.banda.web.timetracker.service.RoleService;
import it.banda.web.timetracker.service.UserService;

import java.util.List;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author a.ferreira
 * @since 08/06/2017
 */

@RestController
public class ActivityController {
	private static final Logger logger = LoggerFactory.getLogger(ActivityController.class);
	
	@Autowired
	private ActivityService activityService;
	
	@Autowired
	private AllocationService allocationService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value = "/activity/new", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> addActivity(@RequestBody Activity activity) {
		logger.info("Creating a new Activity...");
		
		if (activity == null) {
			logger.error("The Activity to save cannot be null!");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		
		try {
			activityService.save(activity);
			
			// TODO - delete the temporary code, used only for test.
			Role role = new Role(null, "BASIC");
			roleService.save(role);
			
			User user = new User();
			user.setUsername("willy"); // The username is 'unique' and it has to be changed every time.
			user.setPassword("test");
			user.setFirstName("William");
			user.setLastName("Campbel");
			user.setEmail("w.campbel@gmail.com"); // email is also 'unique'
			if (role != null) {
				user.setRole(role);
			}
			userService.save(user);
						
			Allocation allocation = new Allocation();
			allocation.setUser(user);
			allocation.setActivity(activity);
			allocationService.save(allocation);
			// TODO - delete the temporary code, used only for test.
			
		} catch (Exception ex) {
			logger.error("Error on saving the new Activity!", ex);
			return new ResponseEntity<Void>(HttpStatus.BAD_GATEWAY);
		}
		
		logger.info("New Activity created successfully.");
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/activity/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Activity> getActivity(@PathVariable Long id) {
		logger.info("Retrieving the Activity with ID '" + id + "' from DB...");
		
		Activity activity = null;
		try {
			activity = activityService.findByIdFetchAllocations(id);
		} catch (EntityNotFoundException ex) {
			logger.error("No Activity found for ID: " + id);
			return new ResponseEntity<Activity>(HttpStatus.NOT_FOUND);
		}
		
		logger.info("Activity with ID '" + id + "' found successfully.");
		return new ResponseEntity<Activity>(activity, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/activities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Activity>> listAllActivities() {
		logger.info("Retrieving the list of all Activities from DB...");
		
		List<Activity> activities = activityService.findAll();
		if (activities == null || activities.isEmpty()) {
			logger.debug("No activity fround!");
			return new ResponseEntity<List<Activity>>(HttpStatus.NO_CONTENT);
		}
		
		logger.info("It was found a total of " + activities.size() + " activity/activities.");
		return new ResponseEntity<List<Activity>>(activities, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/activity/{id}", method = RequestMethod.PUT, 
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Activity> updateActivity(@PathVariable Long id, @RequestBody Activity updatedActivity) {
		logger.info("Updating the Activity with ID '" + id + "' ...");
		
		Activity activity = null;
		try {
			activity = activityService.findByIdFetchAllocations(id);
			activity.setName(updatedActivity.getName());
			activity.setDescription(updatedActivity.getDescription());
			activity.setType(updatedActivity.getType());
			
			if (null != updatedActivity.getAllocations() && updatedActivity.getAllocations().size() > 0) {
				activity.setAllocations(updatedActivity.getAllocations());
			}
			
			activityService.update(activity);
		} catch (EntityNotFoundException ex) {
			logger.error("No Activity found for ID: " + id);
			return new ResponseEntity<Activity>(HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			logger.error("Error on updating the new Activity!", ex);
			return new ResponseEntity<Activity>(HttpStatus.BAD_GATEWAY);
		}
		
		logger.info("Activity updated successfully.");
		return new ResponseEntity<Activity>(activity, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/activity/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteActivity(@PathVariable Long id) {
		logger.debug("Deleting the Activity with ID '" + id + "'");
		
		Activity activity = null;
		try {
			activity = activityService.findById(id);
			activityService.deleteCascade(activity);
		} catch (NoResultException ex) {
			logger.error("No Activity found for ID: " + id);
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			logger.error("Error on deleting the Activity!", ex);
			return new ResponseEntity<Void>(HttpStatus.BAD_GATEWAY);
		}
		
		logger.info("Activity deleted successfully.");
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@RequestMapping(value = "/activity/list/dto/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ActivityListDTO> getActivityList(@PathVariable Long id) {
		logger.info("Retrieving the Activity List with ID '" + id + "' ...");
		
		Activity activity = null;
		ActivityListDTO activityDTOList = new ActivityListDTO();
		try {
			activity= activityService.findByIdFetchAllocations(id);
			
			if (activity == null) {
				logger.debug("No activity fround!");
				return new ResponseEntity<ActivityListDTO>(HttpStatus.NO_CONTENT);
			}
			
			for (Allocation allocation : activity.getAllocations()) {
				Allocation allocForDTO = allocationService.findByIdFetchDays(allocation.getId());
				// TODO ...
			}
			
		} catch (EntityNotFoundException ex) {
			
		}
		
		//logger.info("It was found a total of " + activities.size() + " activity/activities.");
		return new ResponseEntity<ActivityListDTO>(activityDTOList, HttpStatus.OK);
	}
}
