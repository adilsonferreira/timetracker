package it.banda.web.timetracker.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author a.ferreira
 * @since 08/06/2017
 */

public class ActivityListDTO implements Serializable {
	private static final long serialVersionUID = 2964272707328261046L;
	
	private List<ActivityDTO> activityList;
	
	public ActivityListDTO() {
	}

	public List<ActivityDTO> getActivityList() {
		if (activityList == null) {
			activityList = new ArrayList<ActivityDTO>();
		}
		
		return activityList;
	}

	public void setActivityList(List<ActivityDTO> activityList) {
		this.activityList = activityList;
	}
	
}
