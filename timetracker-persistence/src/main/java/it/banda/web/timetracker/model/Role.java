package it.banda.web.timetracker.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author a.ferreira
 * @since 07/06/2017
 */

@Entity
@Table(name = "T_ROLE")
public class Role implements ModelObject {
	private static final long serialVersionUID = -1768128995569190403L;
	
	private Long id;
	private String roleName;
	private Set<User> users;
	private Integer version;
	
	public Role() {
	}
	
	public Role(Long id, String roleName) {
		this.id = id;
		this.roleName = roleName;
	}

	@Id
	@Column(name = "role_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "role_name")
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	@JsonManagedReference
	@OneToMany(mappedBy = "role", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	public Set<User> getUsers() {
		if (this.users == null) {
			this.users = new HashSet<User>();
		}
		
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	@JsonIgnore
	@Version
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
