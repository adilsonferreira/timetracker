package it.banda.web.timetracker.dao.impl;

import it.banda.web.timetracker.dao.ActivityDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Activity;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author a.ferreira
 * @since 06/06/2017
 */

@Repository("activityDao")
public class ActivityDaoImpl extends GenericDaoImpl<Activity> implements ActivityDao {
	private static final Logger logger = LoggerFactory.getLogger(ActivityDaoImpl.class);

	public ActivityDaoImpl() {
		super(Activity.class);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Activity findActivity(String name) throws DataAccessException, EntityNotFoundException {
		logger.info("Finding Activity record from DB with the name '" + name + "' ...");
		Query query = entityManager.createQuery("select at from Activity at where at.name = :name");
		query.setParameter("name", name);
		
		Activity activity = null;
		try {
			activity = (Activity) query.getSingleResult();
		} catch (NoResultException ex) {
			logger.error("No Activity found for name: '" + name + "'" );
			throw new EntityNotFoundException("No Activity found for name: '" + name + "'", ex);
		}
				
		logger.info("Activity '" + name + "' found successfully.");
		return activity;
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Activity> findActivityListFetchAllocations(Long activityId) {
		Query query = entityManager.createQuery("select act from Activity act left join act.allocations where act.id = :id");
		query.setParameter("id", activityId);
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Activity findByIdFetchAllocations(Long activityId) throws EntityNotFoundException {
		Query query = entityManager.createQuery("select distinct act from Activity act left join act.allocations where act.id = :id");
		query.setParameter("id", activityId);
		
		Activity activity = null;
		try {
			activity = (Activity) query.getSingleResult();
		} catch (NoResultException ex) {
			logger.error("No Activity found for ID: '" + activityId + "'" );
			throw new EntityNotFoundException("No Activity found for name: '" + activityId + "'", ex);
		}
				
		logger.info("Activity '" + activityId + "' found successfully.");
		return activity;
	}

	@Override
	@Transactional
	public void deleteCascade(Activity activity) {
		Activity toRemove = entityManager.getReference(Activity.class, activity.getId());
		entityManager.remove(toRemove);
	}

}
