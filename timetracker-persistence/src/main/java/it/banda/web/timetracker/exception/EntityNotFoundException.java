package it.banda.web.timetracker.exception;

public class EntityNotFoundException extends Exception {
	private static final long serialVersionUID = 1912346660101022560L;
	
	public EntityNotFoundException() {
		super();
	}
	
	public EntityNotFoundException(String message) {
		super(message);
	}
	
	public EntityNotFoundException(Throwable throwable) {
		super(throwable);
	}
	
	public EntityNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
}
