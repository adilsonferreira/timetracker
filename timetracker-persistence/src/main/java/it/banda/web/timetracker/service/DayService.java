package it.banda.web.timetracker.service;

import java.util.Date;
import java.util.List;

import it.banda.web.timetracker.dao.DayDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Day;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author a.ferreira
 * @since 13/06/2017
 */

@Service("dayService")
public class DayService implements DayDao {
	
	@Autowired
	private DayDao dayDao;

	@Override
	public Day findById(Long id) {
		return dayDao.findById(id);
	}

	@Override
	public List<Day> findAll() {
		return dayDao.findAll();
	}

	@Override
	public void save(Day day) {
		dayDao.save(day);
	}

	@Override
	public void update(Day day) {
		dayDao.update(day);
	}

	@Override
	public void delete(Day day) {
		dayDao.delete(day);
	}

	@Override
	public Day findDay(Date date) throws EntityNotFoundException {
		return dayDao.findDay(date);
	}
	
}
