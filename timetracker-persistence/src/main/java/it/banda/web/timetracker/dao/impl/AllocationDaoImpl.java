package it.banda.web.timetracker.dao.impl;

import it.banda.web.timetracker.dao.AllocationDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Allocation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

/**
 * @author a.ferreira
 * @since 10/06/2017
 */

@Repository("allocationDao")
public class AllocationDaoImpl extends GenericDaoImpl<Allocation> implements AllocationDao {
	private static final Logger logger = LoggerFactory.getLogger(AllocationDaoImpl.class);
	
	public AllocationDaoImpl() {
		super(Allocation.class);
	}

	@Override
	public Allocation findAllocation(String name) throws DataAccessException, EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Allocation> findAllocationListFetchDays() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Allocation findByIdFetchDays(Long allocationId) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public void deleteAllocationCascade(Allocation allocation) {
//		// TODO Auto-generated method stub
//	}
	
}
