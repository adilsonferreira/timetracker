package it.banda.web.timetracker.dao;

import java.util.Date;

import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Day;

/**
 * @author a.ferreira
 * @since 13/06/2017
 */

public interface DayDao extends GenericDao<Day> {
	public Day findDay(Date date) throws EntityNotFoundException;
}
