package it.banda.web.timetracker.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author a.ferreira
 * @since 07/06/2017
 */

@Entity
@Table(name = "T_DAY")
public class Day implements ModelObject {
	private static final long serialVersionUID = -7391917585063915649L;
	
	private Long id;
	private Date date;
	private Integer hours;
	private String note;
	private Integer minutes;
	private Allocation allocation;
	private Integer version;
	
	public Day() {
	}
	
	public Day(Long id, Date date, Integer hours, String note, Integer minutes,
			Allocation allocation) {
		this.id = id;
		this.date = date;
		this.hours = hours;
		this.minutes = minutes;
		this.allocation = allocation;
		this.note = note;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public Integer getMinutes() {
		return minutes;
	}

	public void setMinutes(Integer minutes) {
		this.minutes = minutes;
	}
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "allocation_id")
	public Allocation getAllocation() {
		return allocation;
	}

	public void setAllocation(Allocation allocation) {
		this.allocation = allocation;
	}
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	@JsonIgnore
	@Version
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
