package it.banda.web.timetracker.service;

import it.banda.web.timetracker.dao.ActivityDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Activity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author a.ferreira
 * @since 06/06/2017
 */

@Service("activityService")
public class ActivityService implements ActivityDao {
	
	@Autowired
	private ActivityDao activityDao;

	@Override
	public Activity findById(Long id) {
		return activityDao.findById(id);
	}

	@Override
	public List<Activity> findAll() {
		return activityDao.findAll();
	}

	@Override
	public void save(Activity activity) {
		activityDao.save(activity);
	}
	
	@Override
	public void update(Activity activity) {
		activityDao.update(activity);
	}

	@Override
	public void delete(Activity activity) {
		activityDao.delete(activity);
	}

	@Override
	public Activity findActivity(String name) throws EntityNotFoundException {
		return activityDao.findActivity(name);
	}

	@Override
	public List<Activity> findActivityListFetchAllocations(Long activityId) {
		return activityDao.findActivityListFetchAllocations(activityId);
	}

	@Override
	public Activity findByIdFetchAllocations(Long activityId) throws EntityNotFoundException {
		return activityDao.findByIdFetchAllocations(activityId);
	}

	@Override
	public void deleteCascade(Activity activity) {
		activityDao.deleteCascade(activity);
	}

}
