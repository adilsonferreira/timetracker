package it.banda.web.timetracker.service;

import it.banda.web.timetracker.dao.AllocationDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Allocation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author a.ferreira
 * @since 10/06/2017
 */

@Service("allocationService")
public class AllocationService implements AllocationDao {
	
	@Autowired
	private AllocationDao allocationDao;
	
	@Override
	public Allocation findById(Long id) {
		return allocationDao.findById(id);
	}

	@Override
	public List<Allocation> findAll() {
		return allocationDao.findAll();
	}

	@Override
	public void save(Allocation allocation) {
		allocationDao.save(allocation);
	}
	
	@Override
	public void update(Allocation allocation) {
		allocationDao.update(allocation);
	}

	@Override
	public void delete(Allocation allocation) {
		allocationDao.delete(allocation);
	}

	@Override
	public Allocation findAllocation(String name) throws EntityNotFoundException {
		return allocationDao.findAllocation(name);
	}

	@Override
	public List<Allocation> findAllocationListFetchDays() {
		return allocationDao.findAllocationListFetchDays();
	}

	@Override
	public Allocation findByIdFetchDays(Long allocationId) throws EntityNotFoundException {
		return allocationDao.findByIdFetchDays(allocationId);
	}

}
