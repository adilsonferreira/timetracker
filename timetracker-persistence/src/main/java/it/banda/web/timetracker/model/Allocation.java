package it.banda.web.timetracker.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author a.ferreira
 * @since 07/06/2017
 */

@Entity
@Table(name = "T_ALLOCATION")
public class Allocation implements ModelObject {
	private static final long serialVersionUID = -1044959594416030455L;
	
	private Long id;
	private User user;
	private Activity activity;
	private Set<Day> days;
	private Integer version;
	
	public Allocation() {
	}

	public Allocation(Long id, User user, Activity activity) {
		this.id = id;
		this.user = user;
		this.activity = activity;
	}

	@Id
	@Column(name = "allocation_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "activity_id", nullable = false)
	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	
	@JsonIgnore
	@JsonManagedReference
	@OneToMany(mappedBy = "allocation", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	public Set<Day> getDays() {
		if (this.days == null) {
			this.days = new HashSet<Day>();
		}
		
		return days;
	}

	public void setDays(Set<Day> days) {
		this.days = days;
	}
	
	@JsonIgnore
	@Version
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
