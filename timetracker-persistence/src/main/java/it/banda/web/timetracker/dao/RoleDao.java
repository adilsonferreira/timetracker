package it.banda.web.timetracker.dao;

import java.util.List;

import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Role;

/**
 * @author a.ferreira
 * @since 12/06/2017
 */

public interface RoleDao extends GenericDao<Role> {
	public Role findRole(String name) throws EntityNotFoundException;
	public Role findByIdFetchUsers(Long roleId) throws EntityNotFoundException;
	public List<Role> findRoleListFetchUsers(Long roleId);
}
