package it.banda.web.timetracker.dao;

import java.util.List;

import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.User;

/**
 * @author a.ferreira
 * @since 08/06/2017
 */

public interface UserDao extends GenericDao<User> {
	public User findUser(String username) throws EntityNotFoundException;
	public User findByIdFetchAllocations(Long userId) throws EntityNotFoundException;
	public List<User> findUserListFetchAllocations();
	public User authenticateUser(String username, String password) throws EntityNotFoundException;
}
