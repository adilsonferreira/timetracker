package it.banda.web.timetracker.dao.impl;

import java.util.Date;

import it.banda.web.timetracker.dao.DayDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Day;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * @author a.ferreira
 * @since 13/06/2017
 */

@Repository("dayDao")
public class DayDaoImpl extends GenericDaoImpl<Day> implements DayDao {
	private static final Logger logger = LoggerFactory.getLogger(DayDaoImpl.class);
	
	public DayDaoImpl() {
		super(Day.class);
	}

	@Override
	public Day findDay(Date date) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
