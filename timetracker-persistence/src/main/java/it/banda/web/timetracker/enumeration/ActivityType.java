package it.banda.web.timetracker.enumeration;

/**
 * @author a.ferreira
 * @since 07/06/2017
 */

public enum ActivityType {
	ORDINARY,
	EXTRAORDINARY;
}
