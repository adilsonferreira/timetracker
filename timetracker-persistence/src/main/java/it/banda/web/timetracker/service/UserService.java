package it.banda.web.timetracker.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.banda.web.timetracker.dao.UserDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.User;

/**
 * @author a.ferreira
 * @since 10/06/2017
 */

@Service("userService")
public class UserService implements UserDao {
	
	@Autowired
	private UserDao userDao;

	@Override
	public User findById(Long id) {
		return userDao.findById(id);
	}

	@Override
	public List<User> findAll() {
		return userDao.findAll();
	}

	@Override
	public void save(User user) {
		userDao.save(user);
	}
	
	@Override
	public void update(User user) {
		userDao.update(user);
	}

	@Override
	public void delete(User user) {
		userDao.delete(user);
	}

	@Override
	public User findUser(String username) throws EntityNotFoundException {
		return userDao.findUser(username);
	}

	@Override
	public User authenticateUser(String username, String password) throws EntityNotFoundException {
		return userDao.authenticateUser(username, password);
	}

	@Override
	public User findByIdFetchAllocations(Long userId) throws EntityNotFoundException {
		return userDao.findByIdFetchAllocations(userId);
	}

	@Override
	public List<User> findUserListFetchAllocations() {
		return userDao.findUserListFetchAllocations();
	}

}
