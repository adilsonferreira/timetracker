package it.banda.web.timetracker.dao.impl;

import it.banda.web.timetracker.dao.GenericDao;
import it.banda.web.timetracker.model.ModelObject;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

/**
 * The implementation of Generic DAO that contains common CRUD operations
 * nedeed in every DAO class.
 * 
 * @author a.ferreira
 * @since 08/06/2017
 *
 * @param <T> The actual model class instantiated
 */

@SuppressWarnings("unchecked")
public class GenericDaoImpl<T extends ModelObject> implements GenericDao<T> {
	private Class<T> type;
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	public GenericDaoImpl(Class<T> type) {
		super();
		this.type = type;
	}

	@Override
	@Transactional(readOnly = true)
	public T findById(Long id) {
		return (T) entityManager.find(type, id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> findAll() {
		return entityManager.createQuery("select obj from " + type.getName() + " as obj").getResultList();
	}

	@Override
	@Transactional
	public void save(T object) {
		entityManager.persist(object);
	}
	
	@Override
	@Transactional
	public void update(T object) {
		entityManager.merge(object);
	}

	@Override
	@Transactional(rollbackFor = DataAccessException.class, readOnly = false)
	public void delete(T object) {
		entityManager.remove(object);
	}
	
}
