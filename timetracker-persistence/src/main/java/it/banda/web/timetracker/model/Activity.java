package it.banda.web.timetracker.model;

import it.banda.web.timetracker.enumeration.ActivityType;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author a.ferreira
 * @since 06/06/2017
 */

@Entity
@Table(name = "T_ACTIVITY")
public class Activity implements ModelObject {
	private static final long serialVersionUID = -7415619902671298636L;
	
	private Long id;
	private String name;
	private String description;
	private ActivityType type;
	private Set<Allocation> allocations;
	private Integer version;
	
	public Activity() {
	}
	
	public Activity(Long id, String name, String description,
			ActivityType type) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.type = type;
	}

	@Id
	@Column(name = "activity_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@NotEmpty
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Enumerated(EnumType.STRING)
	public ActivityType getType() {
		return type;
	}

	public void setType(ActivityType type) {
		this.type = type;
	}
	
	@JsonManagedReference
	@OneToMany(mappedBy = "activity", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	public Set<Allocation> getAllocations() {
		if (this.allocations == null) {
			this.allocations = new HashSet<Allocation>();
		}
		
		return allocations;
	}

	public void setAllocations(Set<Allocation> allocations) {
		this.allocations = allocations;
	}
	
	@JsonIgnore
	@Version
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
